import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo, Form, Label, Input, ButtonWarning } from '../../../styles/custom_adm';
import api from '../../../config/configApi';

export const EditarRl = (props) => {

    const [id] = useState(props.match.params.id);
    const [rl, setRl] = useState('');

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const editRole = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.put("/role", { id, rl }, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Nível de acesso não editado com sucesso!'
                });
            });
    }

    useEffect(() => {
        const getRole = async () => {
            await api.get("/role/" + id)
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setRl(response.data.role.rl);
                    }
                })
                .catch((err) => {
                    
                });
        }

        getRole();
    }, [id]);

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Editar Cargo</Titulo>
                <BotaoAcao>
                    <Link to="/listarRl">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>



            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
                
                <Form onSubmit={editRole}>
                    <Label>Nível de Acesso: {id} </Label>
                    <Input type="text" name="rl" placeholder="Cargo" value={rl} onChange={e => setRl(e.target.value)} />

                    <ButtonWarning type="submit">Editar</ButtonWarning>
                </Form>
            </Conteudo>
        </Container>
    )
}