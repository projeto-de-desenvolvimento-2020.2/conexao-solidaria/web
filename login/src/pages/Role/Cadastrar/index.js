import React, { useState } from 'react';
import { Link } from 'react-router-dom';



import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo, Form, Label, Input, ButtonSuccess } from '../../../styles/custom_adm';

import api from '../../../config/configApi';

export const CadastrarRl = () => {

    const [role, setRole] = useState({
        rl: '',
    });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const valorInput = e => setRole({ ...role, [e.target.name]: e.target.value });

    const CadRole = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.post("/role", role, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                }

            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Cadastrar Cargo</Titulo>
                <BotaoAcao>
                    <Link to="/listarRl" exact>
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={CadRole}>
                    <Label>Nome: </Label>
                    <Input type="text" name="rl" placeholder="Cargo (nível de acesso)" onChange={valorInput} />

                    <ButtonSuccess type="submit">Cadastrar</ButtonSuccess>
                </Form>
            </Conteudo>

        </Container>
    )
}