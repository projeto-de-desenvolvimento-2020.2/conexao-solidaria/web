import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import api from '../../../config/configApi';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, ButtonSuccess, Table, AlertDanger, AlertSuccess, ButtonPrimary, ButtonWarning, ButtonDanger } from '../../../styles/custom_adm';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons'; 
library.add(fas);


export const ListarRl = () => {

    const [data, setData] = useState({});
    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const getRoles = async () => {
        await api.get("/roles")
            .then((response) => {
                console.log(response.data);
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setData(response.data.roles);
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    useEffect(() => {
        getRoles();
    }, []);

    const apagarRole = async (idRole) => {

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.delete("/role/" + idRole, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                    getRoles();
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Nível de acesso possui usuários cadastrados!'
                });
            })
    }

    return (
        <Container>
           
            <ConteudoTitulo>
                <Titulo>Listar Cargos</Titulo>
                <BotaoAcao>
                    <Link to="/cadastrarRl">
                        <ButtonSuccess>Cadastrar</ButtonSuccess>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Cargo</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(data) && data.map(role => (
                        <tr key={role.id}>
                            <td>{role.id}</td>
                            <td>{role.rl}</td>
                            <td>
                                <Link to={"/visualizarRl/" + role.id}>
                                    <ButtonPrimary><FontAwesomeIcon icon="eye"/></ButtonPrimary>
                                </Link>{' '}
                                <Link to={"/editarRl/" + role.id}>
                                    <ButtonWarning><FontAwesomeIcon icon="edit"/></ButtonWarning>
                                </Link>{' '}
                                <Link to={"#"}>
                                    <ButtonDanger onClick={() => apagarRole(role.id)}><FontAwesomeIcon icon="trash-alt"/></ButtonDanger>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    )
}

/*
<Link to={"/visualizarRl/" + role.id}>
<ButtonPrimary>Visualizar</ButtonPrimary>
</Link>{' '}
*/