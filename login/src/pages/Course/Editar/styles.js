import styled from 'styled-components';

export const ConteudoCurso = styled.p`
    color: #3e3e3e;
    font-size: 16px;
`;