import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo, Form, Label, Input, ButtonWarning } from '../../../styles/custom_adm';
import api from '../../../config/configApi';

export const EditarCurso = (props) => {

    const [id] = useState(props.match.params.id);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [ch, setCh] = useState(0);
    const [start_date, setStart_date] = useState('');
    const [final_date, setFinal_date] = useState('');


    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const editCourse = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.put("/course", {id, name, description, ch, start_date, final_date}, {headers})
        .then((response) => {
            if(response.data.erro){
                setStatus({
                    type: 'erro',
                    mensagem: response.data.mensagem
                });
            }else{
                setStatus({
                    type: 'success',
                    mensagem: response.data.mensagem
                });
            }
        }).catch(() => {
            setStatus({
                type: 'erro',
                mensagem: 'Erro: Tente mais tarde!'
            });
        });
    }

    useEffect(() => {
        const getCourse = async () => {
            await api.get("/course/" + id)
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setName(response.data.course.name);
                        setDescription(response.data.course.description);
                        setCh(response.data.course.ch);
                        setStart_date(response.data.course.start_date);
                        setFinal_date(response.data.course.final_date);
                    }
                })
                .catch(() => {
                    setStatus({
                        type: 'erro',
                        mensagem: "Erro: Tente mais tarde!"
                    });
                });
        }

        getCourse();
    }, [id]);

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Editar Curso</Titulo>
                <BotaoAcao>
                    <Link to="/listar-cursos">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}

                <Form onSubmit={editCourse}>
                    <Label>Nome: </Label>
                    <Input type="text" name="name" placeholder="Nome do curso" value={name} onChange={e => setName(e.target.value)} />

                    <Label>Descrição: </Label>
                    <Input type="text" name="description" placeholder="Descrição do curso" value={description} onChange={e => setDescription(e.target.value)} />

                    <Label>Carga Horária: </Label>
                    <Input type="number" name="ch" placeholder="Carga horária do curso" value={ch} autoComplete="on"  onChange={e => setCh(e.target.value)} />

                    <Label>Data de início: </Label>
                    <Input type="text" name="start_date" placeholder="Data de início do curso" value={start_date} autoComplete="on"  onChange={e => setStart_date(e.target.value)} />

                    <Label>Data de término: </Label>
                    <Input type="text" name="start_date" placeholder="Data de término do curso" value={final_date} autoComplete="on"  onChange={e => setFinal_date(e.target.value)} />


                    <ButtonWarning type="submit">Editar</ButtonWarning>
                </Form>
            </Conteudo>
        </Container>
    )
}