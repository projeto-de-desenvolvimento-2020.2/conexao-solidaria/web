import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo, Form, Label, Input, ButtonSuccess } from '../../../styles/custom_adm';

import api from '../../../config/configApi';

export const CadastrarCurso = () => {

    const [course, setCourse] = useState({
        name: '',
        description: '',
        ch: '',
        start_date: '',
        final_date: '',
        });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const valorInput = e => setCourse({ ...course, [e.target.name]: e.target.value });

    const cadCourse = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.post("/course", course, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                }

            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Cadastrar Curso</Titulo>
                <BotaoAcao>
                    <Link to="/listar-cursos" exact>
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={cadCourse}>
                    <Label>Nome: </Label>
                    <Input type="text" name="name" placeholder="Nome do curso" onChange={valorInput} />

                    <Label>Descrição: </Label>
                    <Input type="text" name="description" placeholder="E-mail do curso" onChange={valorInput} />

                    <Label>Carga Horária: </Label>
                    <Input type="number" name="ch" placeholder="Carga horária do curso" autoComplete="on" onChange={valorInput} />

                    <Label>Data de início: </Label>
                    <Input type="text" name="start_date" placeholder="Data de início do curso" autoComplete="on" onChange={valorInput} />

                    <Label>Data de término: </Label>
                    <Input type="text" name="final_date" placeholder="Data de término do curso" autoComplete="on" onChange={valorInput} />

                    <ButtonSuccess type="submit">Cadastrar</ButtonSuccess>
                </Form>
            </Conteudo>

        </Container>
    )
}