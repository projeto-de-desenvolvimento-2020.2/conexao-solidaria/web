import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import api from '../../../config/configApi';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, ButtonSuccess, Table, AlertDanger, AlertSuccess, ButtonPrimary, ButtonWarning, ButtonDanger } from '../../../styles/custom_adm';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons'; 
library.add(fas);

export const ListarCurso = () => {

    const [data, setData] = useState({});
    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const getCourses = async () => {
        await api.get("/courses")
            .then((response) => {
                console.log(response.data);
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setData(response.data.courses);
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    useEffect(() => {
        getCourses();
    }, []);

    const apagarCourse = async (idCourse) => {

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.delete("/course/" + idCourse, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                    getCourses();
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            })
    }

    return (
        <Container>
            <ConteudoTitulo>
                <Titulo>Cursos profissionalizantes - Pároquias</Titulo> <br /> <br />
                <BotaoAcao>
                    <Link to="/cadastrar-curso">
                        <ButtonSuccess>Cadastrar</ButtonSuccess>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th>Carga/H</th>
                        <th>Data de Início</th>
                        <th>Data de Término</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(data) && data.map(course => (
                        <tr key={course.id}>
                            <td>{course.id}</td>
                            <td>{course.name}</td>
                            <td>{course.description}</td>
                            <td>{course.ch}</td>
                            <td>{course.start_date}</td>
                            <td>{course.final_date}</td>
                            <td>
                                <Link to={"/visualizar-curso/" + course.id}>
                                    <ButtonPrimary><FontAwesomeIcon icon="eye"/></ButtonPrimary>
                                </Link>{' '}
                                <Link to={"/editar-curso/" + course.id}>
                                    <ButtonWarning><FontAwesomeIcon icon="edit"/></ButtonWarning>
                                </Link>{' '}
                                <Link to={"#"}>
                                    <ButtonDanger onClick={() => apagarCourse(course.id)}><FontAwesomeIcon icon="trash-alt"/></ButtonDanger>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    )
}