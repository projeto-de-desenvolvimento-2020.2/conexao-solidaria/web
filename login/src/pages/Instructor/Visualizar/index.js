import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo } from '../../../styles/custom_adm';
import { ConteudoUsuario } from './styles';

import api from '../../../config/configApi';

export const VisualizarInstrutor = (props) => {

    const [data, setData] = useState([]);
    const [id] = useState(props.match.params.id);

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    useEffect(() => {
        const getUser = async () => {
            await api.get("/instructor/" + id)
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setData(response.data.instructor);
                    }
                })
                .catch(() => {
                    setStatus({
                        type: 'erro',
                        mensagem: "Erro: Tente mais tarde!"
                    });
                });
        }

        getUser();
    }, [id]);

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Visualizar Instrutor</Titulo>
                <BotaoAcao>
                    <Link to="/listar-instrutores">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}

                <ConteudoUsuario>Nome: {data.name}</ConteudoUsuario>
                <ConteudoUsuario>E-mail: {data.email}</ConteudoUsuario>
                <ConteudoUsuario>User_ID: {data.id}</ConteudoUsuario>
                
            </Conteudo>
        </Container>
    )
}