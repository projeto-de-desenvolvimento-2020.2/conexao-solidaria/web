import React, { useState, useContext, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, FormLogin, Titulo, Input, ButtonPrimary, AlertDanger, AlertSuccess } from './styles';
import { Context } from '../../Context/AuthContext';
import api from '../../config/configApi';

import ReCAPTCHA from 'react-google-recaptcha';

export const Login = () => {

    const history = useHistory();

    const { signIn } = useContext(Context);

    const [dataUser, setDataUser] = useState({
        email: '',
        password: ''
    });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const [validCaptcha, setValidCaptcha] = useState(null);
    const [validUser, setValidUser] = useState(false);

    const captcha = useRef(null);

    const onChange = () => {
        if (captcha.current.getValue()) {
            setValidUser(true);
            setValidCaptcha(true);
        }
    }

    const valorInput = e => setDataUser({ ...dataUser, [e.target.name]: e.target.value });

    const loginSubmit = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        };

        if (validUser) {

            if (captcha.current.getValue()) {
                setValidUser(true);
                setValidCaptcha(true);
            }

            api.post("/login", dataUser, { headers })
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setStatus({
                            type: 'success',
                            mensagem: response.data.mensagem
                        });
                        // Salvar o token localStorage
                        localStorage.setItem('token', JSON.stringify(response.data.token));
                        api.defaults.headers.Authorization = `Bearer ${response.data.token}`;
                        signIn(true);

                        return history.push('/dashboard');
                    }
                }).catch(() => {
                    setStatus({
                        type: 'erro',
                        mensagem: "Erro: Usuário ou senha incorreta!"
                    });
                });
        }
    }

    return (
        <Container>
            <FormLogin>
                <p align="center"><img width="60px" text-center src="public/../../logo.png" alt="Conexão Solidária" /> </p>
                <Titulo>Conexão Solidária</Titulo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}

                <form onSubmit={loginSubmit}>
                    <Input type="text" name="email" placeholder="E-mail" onChange={valorInput} />

                    <Input type="password" name="password" placeholder="Senha" autoComplete="on" onChange={valorInput} /><br />

                    <div className="recaptcha">

                        <ReCAPTCHA
                            ref={captcha}
                            sitekey='6LcIh2IbAAAAAF48ndKY4NxzINPXDC8o0Zo8eoV0'
                            onChange={onChange}
                        />
                    </div>

                    <br />

                    {validCaptcha === false && <div className="error-captcha">{setStatus({
                        type: 'erro',
                        mensagem: 'Informe o captcha'
                    })}</div>}

                   
                    <ButtonPrimary type="submit">Acessar</ButtonPrimary>
                </form>
            </FormLogin>
        </Container>
    );
}