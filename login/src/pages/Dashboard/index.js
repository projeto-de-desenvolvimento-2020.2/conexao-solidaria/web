import React from 'react';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

export default function Dashboard() {
    return (
        <>
            <div className="d-flex">
                <div className="mr-auto p-2">
                    <h2 className="display-4 titulo">Dashboard</h2>
                </div>
            </div>
            <hr />
            <div className="row mb-3">
                <div className="col-lg-3 col-sm-6">
                    <div className="card bg-success text-white">
                        <div className="card-body">
                            <FontAwesomeIcon icon="users" />
                            <h6 className="card-title">Usuários</h6>
                            <h2 className="lead">CS</h2>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="card bg-danger text-white">
                        <div className="card-body">
                            <FontAwesomeIcon icon="church" />
                            <h6 className="card-title">Paróquias e Comunidades</h6>
                            <h2 className="lead">CS</h2>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="card bg-warning text-white">
                        <div className="card-body">
                            <FontAwesomeIcon icon="church" />
                            <h6 className="card-title">Cursos Profissionalizantes</h6>
                            <h2 className="lead">CS</h2>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="card bg-info text-white">
                        <div className="card-body">
                            <FontAwesomeIcon icon="church" />
                            <h6 className="card-title">Serviçosde de autônomos...</h6>
                            <h2 className="lead">CS</h2>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );

}