import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import api from '../../../config/configApi';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, ButtonSuccess, Table, AlertDanger, AlertSuccess, ButtonPrimary, ButtonWarning, ButtonDanger } from '../../../styles/custom_adm';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

export const ListarTurmas = () => {

    const [data, setData] = useState({});
    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const getClasses = async () => {
        await api.get("/classes")
            .then((response) => {
                console.log(response.data);
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setData(response.data.classes);
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    useEffect(() => {
        getClasses();
    }, []);

    const apagarClasse = async (idClasse) => {

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.delete("/classe/" + idClasse, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                    getClasses();
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            })
    }

    return (
        <Container>
            <ConteudoTitulo>
                <Titulo>Listar Turmas</Titulo>
                <BotaoAcao>
                    <Link to="/cadastrar">
                        <ButtonSuccess>Cadastrar</ButtonSuccess>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Curso</th>
                        <th>Turma</th>
                        <th>Capacidade</th>
                        <th>Horário</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(data) && data.map(classe => (
                        <tr key={classe.id}>
                            <td>{classe.id}</td>
                            <td>{classe.course.name}</td>
                            <td>{classe.name}</td>
                            <td>{classe.capacity}</td>
                            <td>{classe.schedule}</td>
                            <td>
                                <Link to={"/visualizar/" + classe.id}>
                                    <ButtonPrimary><FontAwesomeIcon icon="eye" /></ButtonPrimary>
                                </Link>{' '}
                                <Link to={"/editar/" + classe.id}>
                                    <ButtonWarning><FontAwesomeIcon icon="edit" /></ButtonWarning>
                                </Link>{' '}
                                <Link to={"#"}>
                                    <ButtonDanger onClick={() => apagarClasse(classe.id)}><FontAwesomeIcon icon="trash-alt" /></ButtonDanger>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    )
}