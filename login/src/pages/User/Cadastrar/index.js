import React, { useState } from 'react';
import { Link } from 'react-router-dom';



import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo, Form, Label, Input, ButtonSuccess } from '../../../styles/custom_adm';

import api from '../../../config/configApi';

export const Cadastrar = () => {

    const [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        phone: '',
                role_id: 0
    });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const valorInput = e => setUser({ ...user, [e.target.name]: e.target.value });

    const cadUser = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.post("/user", user, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                }

            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    return (
        <Container>



            <ConteudoTitulo>
                <Titulo>Cadastrar Usuário</Titulo>
                <BotaoAcao>
                    <Link to="/listar">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={cadUser}>
                    <Label>Nome: </Label>
                    <Input type="text" name="name" placeholder="Nome do usuário" onChange={valorInput} />

                    <Label>E-mail: </Label>
                    <Input type="email" name="email" placeholder="E-mail do usuário" onChange={valorInput} />

                    <Label>Telefone: </Label>
                    
                    <Input type="text" name="phone" placeholder="Telefone do usuário" maxLength="15" onChange={valorInput} />

                    <Label>Senha: </Label>
                    <Input type="password" name="password" placeholder="Senha para acessar o administrativo" autoComplete="on" onChange={valorInput} />

                    <Label>Role: </Label>
                    <Input type="number" name="role_id" placeholder="Role do usuário" onChange={valorInput} />

                    <ButtonSuccess type="submit">Cadastrar</ButtonSuccess>
                </Form>
            </Conteudo>

        </Container>
    )
}