import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import api from '../../../config/configApi';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, ButtonSuccess, Table, AlertDanger, AlertSuccess, ButtonPrimary, ButtonWarning, ButtonDanger } from '../../../styles/custom_adm';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons'; 
library.add(fas);

export const Listar = () => {

    const [data, setData] = useState({});
    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const getUsers = async () => {
        await api.get("/users")
            .then((response) => {
                console.log(response.data);
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setData(response.data.users);
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            });
    }

    useEffect(() => {
        getUsers();
    }, []);

    const apagarUser = async (idUser) => {

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.delete("/user/" + idUser, { headers })
            .then((response) => {
                if (response.data.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: response.data.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: response.data.mensagem
                    });
                    getUsers();
                }
            })
            .catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Erro: Tente mais tarde!'
                });
            })
    }

    return (
        <Container>
            <ConteudoTitulo>
                <Titulo>Listar Usuários</Titulo>
                <BotaoAcao>
                    <Link to="/cadastrar">
                        <ButtonSuccess>Cadastrar</ButtonSuccess>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Role_ID</th>
                        <th>Role</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Ações</th>
                        
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(data) && data.map(user => (
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.role_id}</td>
                            <td>{user.role.rl}</td>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            
                            <td>
                                <Link to={"/visualizar/" + user.id}>
                                    <ButtonPrimary><FontAwesomeIcon icon="eye"/></ButtonPrimary>
                                </Link>{' '}
                                <Link to={"/editar/" + user.id}>
                                    <ButtonWarning><FontAwesomeIcon icon="edit"/></ButtonWarning>
                                </Link>{' '}
                                <Link to={"#"}>
                                    <ButtonDanger onClick={() => apagarUser(user.id)}><FontAwesomeIcon icon="trash-alt"/></ButtonDanger>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    )
}