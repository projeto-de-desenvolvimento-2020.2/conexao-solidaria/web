SEQUÊNCIA PARA CRIAR O PROJETO
Criar o projeto React - https://pt-br.reactjs.org/docs/create-a-new-react-app.html
### npx create-react-app my-app

Acessar o diretório do projeto
### cd my-app

Rodar o projeto React
### npm start

Gerenciar as rotas
### npm install --save react-router-dom

Realizar chamada para API
### npm install --save axios

Instalar uma versão específica de uma biblioteca
### npm install [<@scope>/]<name>@<version>

Biblioteca utilizada para mudar de página e renderizar a nova página sem recarregar toda a aplicação 
### npm install --save history@4.9.0

Transforma o CSS em componentes
### npm install --save styled-components

Integrar o Reactstrap e o Bootstrap com React
### npm install --save bootstrap
### npm install --save reactstrap react react-dom

//Acrescentar ícone no sistema administrativo
### npm i --save @fortawesome/fontawesome-svg-core  @fortawesome/free-solid-svg-icons @fortawesome/react-fontawesome
